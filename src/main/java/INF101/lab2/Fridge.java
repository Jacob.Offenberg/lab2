package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;



public class Fridge implements IFridge {

    ArrayList<FridgeItem> fridgeItems = new ArrayList<FridgeItem>();

    public void emptyFridge(){
        fridgeItems.removeAll(fridgeItems);
    }
    public int totalSize(){
        return 20;
    }
    public int nItemsInFridge(){
        return fridgeItems.size();
    }
    public boolean placeIn(FridgeItem item){
        if(nItemsInFridge() != totalSize()){
            fridgeItems.add(item);
            return true;
        }
        else{
            return false;
        }
    }
    public void takeOut(FridgeItem item){
        if(!fridgeItems.remove(item)){
            throw new NoSuchElementException("Dette har du ikke!"); 
        }
    }
    public List<FridgeItem> removeExpiredFood(){
        List<FridgeItem> expiredFood = new ArrayList<>();
        for(FridgeItem item: fridgeItems){
            if(item.hasExpired()){
                expiredFood.add(item);
            }
        }
        fridgeItems.removeAll(expiredFood);
        return expiredFood;
    } 

    }
    


